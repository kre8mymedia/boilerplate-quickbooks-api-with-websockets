<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 1)->create([
            'name' => 'John Test',
            'email' => 'john@test.com'
        ]);
        factory(App\User::class, 1)->create([
            'name' => 'Henry Test',
            'email' => 'henry@test.com'
        ]);
        factory(App\User::class, 1)->create([
            'name' => 'Larry Test',
            'email' => 'larry@test.com'
        ]);
    }
}
