<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

// Models
use App\Organization;
use App\User;
// Helpers
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Organization::class, function (Faker $faker) {
    return [
        'primary_user_id' => User::inRandomOrder()->first()->id,
        'uuid' => Str::random(32),
        'name' => $faker->company,
        'address' => $faker->streetAddress,
        'city' => $faker->city,
        'state' => $faker->state,
        'country' => $faker->country,
        'postal_code' => $faker->postcode
    ];
});

