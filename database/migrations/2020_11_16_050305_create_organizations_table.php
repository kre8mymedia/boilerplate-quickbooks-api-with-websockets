<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('uuid', 32)->unique(); // Random String ID
            $table->string('name')->nullable();
            $table->bigInteger('primary_user_id')->nullable(); // The primary owner of this organiziton
            $table->mediumText('address')->nullable(); //The address of the organization
            $table->mediumText('city')->nullable(); //City of the organizaiton
            $table->mediumText('state')->nullable(); //State or province of the organization
            $table->mediumText('country')->nullable(); // The country of the organization
            $table->mediumText('postal_code')->nullable(); // The postal code of the organization
            $table->string('industry')->nullable();
            $table->string('company_size')->nullable();
            $table->string('company_revenue')->nullable();
            $table->string('target_verticles')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations');
    }
}
