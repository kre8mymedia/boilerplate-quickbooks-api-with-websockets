<?php

use App\Events\WebsocketDemoEvent;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', function () {
    broadcast(new WebsocketDemoEvent('some data'));
    return view('welcome');
});


/*
|--------------------------------------------------------------------------
| Chat Routes
|--------------------------------------------------------------------------
*/
Route::get('/chats', 'ChatsController@index');
Route::get('/messages', 'ChatsController@fetchMessages');
Route::post('/messages', 'ChatsController@sendMessage');


/*
|--------------------------------------------------------------------------
| Page Routes
|--------------------------------------------------------------------------
*/
Route::get('users', 'PagesController@dashboard');


/*
|--------------------------------------------------------------------------
| Roles
|--------------------------------------------------------------------------
*/
Route::get('/roles/edit', 'RolesController@index_page')->middleware('auth');
Route::resource('roles', 'RolesController');

/*
|--------------------------------------------------------------------------
| Quickbooks
|--------------------------------------------------------------------------
*/
Route::get('refresh_token', 'QuickbooksController@refresh');
Route::get('get_tokens', 'QuickbooksController@getTokens');
Route::get('get_account', 'QuickbooksController@getAccount');

Route::get('/quickbooks/company_info', 'QuickbooksController@getCompanyInfo')->middleware('auth');
Route::get('/quickbooks/company_pref', 'QuickbooksController@getCompanyPrefs')->middleware('auth');
Route::get('qb_login', 'QuickbooksController@index')->middleware('auth');

/*
|--------------------------------------------------------------------------
| Quickbooks Accounts
|--------------------------------------------------------------------------
*/
// PAGES
Route::get('accounts', 'QuickbooksController@account_page')->middleware('auth');
Route::get('accounts/{id}/edit', 'QuickbooksController@account_edit_page')->middleware('auth');
// CRUD
Route::get('qb_accounts', 'QuickbooksController@accounts')->middleware('auth');
Route::get('qb_accounts/{id}', 'QuickbooksController@account_show')->middleware('auth');
Route::post('qb_accounts', 'QuickbooksController@account_create')->middleware('auth');
Route::put('qb_accounts/{id}', 'QuickbooksController@account_update')->middleware('auth');
Route::delete('qb_accounts/{id}', 'QuickbooksController@account_delete')->middleware('auth');

/*
|--------------------------------------------------------------------------
| Quickbooks Bills
|--------------------------------------------------------------------------
*/
// PAGES
Route::get('bills', 'QuickbooksController@bill_page')->middleware('auth');
Route::get('bills/{id}/edit', 'QuickbooksController@bill_edit_page')->middleware('auth');
// CRUD
Route::get('qb_bills', 'QuickbooksController@bills')->middleware('auth');
Route::get('qb_bills/{id}', 'QuickbooksController@bill_show')->middleware('auth');
Route::post('qb_bills', 'QuickbooksController@bill_create')->middleware('auth');
Route::put('qb_bills/{id}', 'QuickbooksController@bill_update')->middleware('auth');
Route::delete('qb_bills/{id}', 'QuickbooksController@bill_delete')->middleware('auth');

/*
|--------------------------------------------------------------------------
| Quickbooks Bill Payments
|--------------------------------------------------------------------------
*/
// PAGES
Route::get('bill_payments', 'QuickbooksController@bill_payment_page')->middleware('auth');
Route::get('bill_payments/{id}/edit', 'QuickbooksController@bill_payment_edit_page')->middleware('auth');
// CRUD
Route::get('qb_bill_payments', 'QuickbooksController@bill_payments')->middleware('auth');
Route::get('qb_bill_payments/{id}', 'QuickbooksController@bill_payment_show')->middleware('auth');
Route::post('qb_bill_payments', 'QuickbooksController@bill_payment_create')->middleware('auth');
Route::put('qb_bill_payments/{id}', 'QuickbooksController@bill_payment_update')->middleware('auth');
Route::delete('qb_bill_payments/{id}', 'QuickbooksController@bill_payment_delete')->middleware('auth');

/*
|--------------------------------------------------------------------------
| Quickbooks Customers
|--------------------------------------------------------------------------
*/
// PAGES
Route::get('customers', 'QuickbooksController@customer_page')->middleware('auth');
Route::get('customers/{id}/edit', 'QuickbooksController@customer_edit_page')->middleware('auth');
// CRUD
Route::get('qb_customers', 'QuickbooksController@customers')->middleware('auth');
Route::get('qb_customers/{id}', 'QuickbooksController@customer_show')->middleware('auth');
Route::post('qb_customers', 'QuickbooksController@customer_create')->middleware('auth');
Route::put('qb_customers/{id}', 'QuickbooksController@customer_update')->middleware('auth');
Route::delete('qb_customers/{id}', 'QuickbooksController@customer_delete')->middleware('auth');

/*
|--------------------------------------------------------------------------
| Quickbooks Estimates
|--------------------------------------------------------------------------
*/
// PAGES
Route::get('estimates', 'QuickbooksController@estimate_page')->middleware('auth');
Route::get('estimates/{id}/edit', 'QuickbooksController@estimate_edit_page')->middleware('auth');
// CRUD
Route::get('qb_estimates', 'QuickbooksController@estimates')->middleware('auth');
Route::get('qb_estimates/{id}', 'QuickbooksController@estimate_show')->middleware('auth');
Route::post('qb_estimates', 'QuickbooksController@estimate_create')->middleware('auth');
Route::put('qb_estimates/{id}', 'QuickbooksController@estimate_update')->middleware('auth');
Route::delete('qb_estimates/{id}', 'QuickbooksController@estimate_delete')->middleware('auth');

/*
|--------------------------------------------------------------------------
| Quickbooks Invoices
|--------------------------------------------------------------------------
*/
// PAGES
Route::get('invoices', 'QuickbooksController@invoice_page')->middleware('auth');
Route::get('invoices/{id}/edit', 'QuickbooksController@invoice_edit_page')->middleware('auth');
// CRUD
Route::get('qb_invoices', 'QuickbooksController@invoices')->middleware('auth');
Route::get('qb_invoices/{id}', 'QuickbooksController@invoice_show')->middleware('auth');
Route::post('qb_invoices', 'QuickbooksController@invoice_create')->middleware('auth');
Route::put('qb_invoices/{id}', 'QuickbooksController@invoice_update')->middleware('auth');
Route::delete('qb_invoices/{id}', 'QuickbooksController@invoice_delete')->middleware('auth');

/*
|--------------------------------------------------------------------------
| Quickbooks Items
|--------------------------------------------------------------------------
*/
// PAGES
Route::get('items', 'QuickbooksController@item_page')->middleware('auth');
Route::get('items/{id}/edit', 'QuickbooksController@item_edit_page')->middleware('auth');
// CRUD
Route::get('qb_items', 'QuickbooksController@items')->middleware('auth');
Route::get('qb_items/{id}', 'QuickbooksController@item_show')->middleware('auth');
Route::post('qb_items', 'QuickbooksController@item_create')->middleware('auth');
Route::put('qb_items/{id}', 'QuickbooksController@item_update')->middleware('auth');
Route::delete('qb_items/{id}', 'QuickbooksController@item_delete')->middleware('auth');

/*
|--------------------------------------------------------------------------
| Quickbooks Journal Entries
|--------------------------------------------------------------------------
*/
// PAGES
Route::get('journal_entries', 'QuickbooksController@journal_entry_page')->middleware('auth');
Route::get('journal_entries/{id}/edit', 'QuickbooksController@journal_entry_edit_page')->middleware('auth');
// CRUD
Route::get('qb_journal_entries', 'QuickbooksController@journal_entries')->middleware('auth');
Route::get('qb_journal_entries/{id}', 'QuickbooksController@journal_entry_show')->middleware('auth');
Route::post('qb_journal_entries', 'QuickbooksController@journal_entry_create')->middleware('auth');
Route::put('qb_journal_entries/{id}', 'QuickbooksController@journal_entry_update')->middleware('auth');
Route::delete('qb_journal_entries/{id}', 'QuickbooksController@journal_entry_delete')->middleware('auth');

/*
|--------------------------------------------------------------------------
| Quickbooks Payments
|--------------------------------------------------------------------------
*/
// PAGES
Route::get('payments', 'QuickbooksController@payment_page')->middleware('auth');
Route::get('payments/{id}/edit', 'QuickbooksController@payment_edit_page')->middleware('auth');
// CRUD
Route::get('qb_payments', 'QuickbooksController@payments')->middleware('auth');
Route::get('qb_payments/{id}', 'QuickbooksController@payment_show')->middleware('auth');
Route::post('qb_payments', 'QuickbooksController@payment_create')->middleware('auth');
Route::put('qb_payments/{id}', 'QuickbooksController@payment_update')->middleware('auth');
Route::delete('qb_payments/{id}', 'QuickbooksController@payment_delete')->middleware('auth');

/*
|--------------------------------------------------------------------------
| Quickbooks Reports
|--------------------------------------------------------------------------
*/
// PAGES
Route::get('reports', 'QuickbooksController@report_page')->middleware('auth');
Route::get('reports/{id}/edit', 'QuickbooksController@report_edit_page')->middleware('auth');
// CRUD
Route::get('qb_reports', 'QuickbooksController@reports')->middleware('auth');
Route::get('qb_reports/{id}', 'QuickbooksController@report_show')->middleware('auth');
Route::post('qb_reports', 'QuickbooksController@report_create')->middleware('auth');
Route::put('qb_reports/{id}', 'QuickbooksController@report_update')->middleware('auth');
Route::delete('qb_reports/{id}', 'QuickbooksController@report_delete')->middleware('auth');

/*
|--------------------------------------------------------------------------
| Quickbooks Time Activities
|--------------------------------------------------------------------------
*/
// PAGES
Route::get('time_activities', 'QuickbooksController@time_activity_page')->middleware('auth');
Route::get('time_activities/{id}/edit', 'QuickbooksController@time_activity_edit_page')->middleware('auth');
// CRUD
Route::get('qb_time_activities', 'QuickbooksController@time_activities')->middleware('auth');
Route::get('qb_time_activities/{id}', 'QuickbooksController@time_activitiy_show')->middleware('auth');
Route::post('qb_time_activities', 'QuickbooksController@time_activitiy_create')->middleware('auth');
Route::put('qb_time_activities/{id}', 'QuickbooksController@time_activitiy_update')->middleware('auth');
Route::delete('qb_time_activities/{id}', 'QuickbooksController@time_activitiy_delete')->middleware('auth');

/*
|--------------------------------------------------------------------------
| Quickbooks Vendors
|--------------------------------------------------------------------------
*/
// PAGES
Route::get('vendors', 'QuickbooksController@vendor_page')->middleware('auth');
Route::get('vendors/{id}/edit', 'QuickbooksController@vendor_edit_page')->middleware('auth');
// CRUD
Route::get('qb_vendors', 'QuickbooksController@vendors')->middleware('auth');
Route::get('qb_vendors/{id}', 'QuickbooksController@vendor_show')->middleware('auth');
Route::post('qb_vendors', 'QuickbooksController@vendor_create')->middleware('auth');
Route::put('qb_vendors/{id}', 'QuickbooksController@vendor_update')->middleware('auth');
Route::delete('qb_vendors/{id}', 'QuickbooksController@vendor_delete')->middleware('auth');

/*
|--------------------------------------------------------------------------
| Quickbooks Vendor Credits
|--------------------------------------------------------------------------
*/
// PAGES
Route::get('vendor_credits', 'QuickbooksController@vendor_credit_page')->middleware('auth');
Route::get('vendor_credits/{id}/edit', 'QuickbooksController@vendor_credit_edit_page')->middleware('auth');
// CRUD
Route::get('qb_vendor_credits', 'QuickbooksController@vendor_credits')->middleware('auth');
Route::get('qb_vendor_credits/{id}', 'QuickbooksController@vendor_credit_show')->middleware('auth');
Route::post('qb_vendor_credits', 'QuickbooksController@vendor_credit_create')->middleware('auth');
Route::put('qb_vendor_credits/{id}', 'QuickbooksController@vendor_credit_update')->middleware('auth');
Route::delete('qb_vendor_credits/{id}', 'QuickbooksController@vendor_credit_delete')->middleware('auth');