<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Sanctum Login
Route::post('/login', 'api\v1\LoginController@login');
Route::post('/register', 'api\v1\RegisterController@register');

// Check Sanctum Token
Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('/users', 'api\v1\user\UserController@index'); // Test Sanctum
    Route::get('/qb_login', 'QuickbooksController@index'); // OAuth2 to get API Token
    
    /*
    |--------------------------------------------------------------------------
    | Quickbooks Company
    |--------------------------------------------------------------------------
    |
    */
    Route::get('/quickbooks/company_info', 'QuickbooksController@getCompanyInfo');
    Route::get('/quickbooks/company_pref', 'QuickbooksController@getCompanyPrefs');

    /*
    |--------------------------------------------------------------------------
    | Quickbooks Accouts
    |--------------------------------------------------------------------------
    |
    */
    Route::get('qb_accounts', 'QuickbooksController@accounts');
    Route::get('qb_accounts/{id}', 'QuickbooksController@account_show');
    Route::post('qb_accounts', 'QuickbooksController@account_create');
    Route::put('qb_accounts/{id}', 'QuickbooksController@account_update');
    // Route::delete('qb_accounts/{id}', 'QuickbooksController@account_delete'); // Not supported render Active: false

    /*
    |--------------------------------------------------------------------------
    | Quickbooks Bills
    |--------------------------------------------------------------------------
    |
    */
    Route::get('qb_bills', 'QuickbooksController@bills');
    Route::get('qb_bills/{id}', 'QuickbooksController@bill_show');
    Route::post('qb_bills', 'QuickbooksController@bill_create');
    Route::put('qb_bills/{id}', 'QuickbooksController@bill_update');
    Route::delete('qb_bills/{id}', 'QuickbooksController@bill_delete');

    /*
    |--------------------------------------------------------------------------
    | Quickbooks Bill Payments
    |--------------------------------------------------------------------------
    |
    */
    Route::get('qb_bill_payments', 'QuickbooksController@bill_payments');
    Route::get('qb_bill_payments/{id}', 'QuickbooksController@bill_payment_show');
    Route::post('qb_bill_payments', 'QuickbooksController@bill_payment_create');
    Route::put('qb_bill_payments/{id}', 'QuickbooksController@bill_payment_update');
    Route::delete('qb_bill_payments/{id}', 'QuickbooksController@bill_payment_delete');

    /*
    |--------------------------------------------------------------------------
    | Quickbooks Customers
    |--------------------------------------------------------------------------
    |
    */
    Route::get('qb_customers', 'QuickbooksController@customers');
    Route::get('qb_customers/{id}', 'QuickbooksController@customer_show');
    Route::post('qb_customers', 'QuickbooksController@customer_create');
    Route::put('qb_customers/{id}', 'QuickbooksController@customer_update');
    Route::delete('qb_customers/{id}', 'QuickbooksController@customer_delete');

    /*
    |--------------------------------------------------------------------------
    | Quickbooks Employees
    |--------------------------------------------------------------------------
    |
    */
    Route::get('qb_employees', 'QuickbooksController@employees');
    Route::get('qb_employees/{id}', 'QuickbooksController@employee_show');
    Route::post('qb_employees', 'QuickbooksController@employee_create');
    Route::put('qb_employees/{id}', 'QuickbooksController@employee_update');
    Route::delete('qb_employees/{id}', 'QuickbooksController@employee_delete');

    /*
    |--------------------------------------------------------------------------
    | Quickbooks Estimates
    |--------------------------------------------------------------------------
    |
    */
    Route::get('qb_estimates', 'QuickbooksController@estimates');
    Route::get('qb_estimates/{id}', 'QuickbooksController@estimate_show');
    Route::post('qb_estimates', 'QuickbooksController@estimate_create');
    Route::put('qb_estimates/{id}', 'QuickbooksController@estimate_update');
    Route::delete('qb_estimates/{id}', 'QuickbooksController@estimate_delete');

    /*
    |--------------------------------------------------------------------------
    | Quickbooks Invoices
    |--------------------------------------------------------------------------
    |
    */
    Route::get('qb_invoices', 'QuickbooksController@invoices');
    Route::get('qb_invoices/{id}', 'QuickbooksController@invoice_show');
    Route::post('qb_invoices', 'QuickbooksController@invoice_create');
    Route::put('qb_invoices/{id}', 'QuickbooksController@invoice_update');
    Route::delete('qb_invoices/{id}', 'QuickbooksController@invoice_delete');

    /*
    |--------------------------------------------------------------------------
    | Quickbooks Items
    |--------------------------------------------------------------------------
    |
    */
    Route::get('qb_items', 'QuickbooksController@items');
    Route::get('qb_items/{id}', 'QuickbooksController@item_show');
    Route::post('qb_items', 'QuickbooksController@item_create');
    Route::put('qb_items/{id}', 'QuickbooksController@item_update');
    Route::delete('qb_items/{id}', 'QuickbooksController@item_delete');

    /*
    |--------------------------------------------------------------------------
    | Quickbooks Journal Entires
    |--------------------------------------------------------------------------
    |
    */
    Route::get('qb_journal_entries', 'QuickbooksController@journal_entries');
    Route::get('qb_journal_entries/{id}', 'QuickbooksController@journal_entry_show');
    Route::post('qb_journal_entries', 'QuickbooksController@journal_entry_create');
    Route::put('qb_journal_entries/{id}', 'QuickbooksController@journal_entry_update');
    Route::delete('qb_journal_entries/{id}', 'QuickbooksController@journal_entry_delete');

    /*
    |--------------------------------------------------------------------------
    | Quickbooks Payments
    |--------------------------------------------------------------------------
    |
    */
    Route::get('qb_payments', 'QuickbooksController@payments');
    Route::get('qb_payments/{id}', 'QuickbooksController@payment_show');
    Route::post('qb_payments', 'QuickbooksController@payment_create');
    Route::put('qb_payments/{id}', 'QuickbooksController@payment_update');
    Route::delete('qb_payments/{id}', 'QuickbooksController@payment_delete');

    /*
    |--------------------------------------------------------------------------
    | Quickbooks Preferences
    |--------------------------------------------------------------------------
    |
    */
    Route::prefix('/quickbooks')->group( function () {
        Route::get('preferences', 'QuickbooksController@getCompanyPrefs');
        Route::put('preferences', 'QuickbooksController@updateCompanyPrefs');
    });

    /*
    |--------------------------------------------------------------------------
    | Quickbooks Reports
    |--------------------------------------------------------------------------
    |
    */
    Route::get('qb_reports', 'QuickbooksController@reports');
    Route::get('qb_reports/{id}', 'QuickbooksController@report_show');
    Route::post('qb_reports', 'QuickbooksController@report_create');
    Route::put('qb_reports/{id}', 'QuickbooksController@report_update');
    Route::delete('qb_reports/{id}', 'QuickbooksController@report_delete');

    /*
    |--------------------------------------------------------------------------
    | Quickbooks Time Activities
    |--------------------------------------------------------------------------
    |
    */
    Route::get('qb_time_activities', 'QuickbooksController@time_activities');
    Route::get('qb_time_activities/{id}', 'QuickbooksController@time_activitiy_show');
    Route::post('qb_time_activities', 'QuickbooksController@time_activitiy_create');
    Route::put('qb_time_activities/{id}', 'QuickbooksController@time_activitiy_update');
    Route::delete('qb_time_activities/{id}', 'QuickbooksController@time_activitiy_delete');

    /*
    |--------------------------------------------------------------------------
    | Quickbooks Vendors
    |--------------------------------------------------------------------------
    |
    */
    Route::get('qb_vendors', 'QuickbooksController@vendors');
    Route::get('qb_vendors/{id}', 'QuickbooksController@vendor_show');
    Route::post('qb_vendors', 'QuickbooksController@vendor_create');
    Route::put('qb_vendors/{id}', 'QuickbooksController@vendor_update');
    Route::delete('qb_vendors/{id}', 'QuickbooksController@vendor_delete');

    /*
    |--------------------------------------------------------------------------
    | Quickbooks Vendor Credits
    |--------------------------------------------------------------------------
    |
    */
    Route::get('qb_vendor_credits', 'QuickbooksController@vendor_credits');
    Route::get('qb_vendor_credits/{id}', 'QuickbooksController@vendor_credit_show');
    Route::post('qb_vendor_credits', 'QuickbooksController@vendor_credit_create');
    Route::put('qb_vendor_credits/{id}', 'QuickbooksController@vendor_credit_update');
    Route::delete('qb_vendor_credits/{id}', 'QuickbooksController@vendor_credit_delete');
});

/*
|--------------------------------------------------------------------------
| Roles
|--------------------------------------------------------------------------
|
*/
// Route::get('roles', 'RolesController@index');
// Route::get('roles/{id}', 'RolesController@show');
Route::post('roles', 'RolesController@store');
Route::delete('roles/{id}', 'RolesController@destroy');