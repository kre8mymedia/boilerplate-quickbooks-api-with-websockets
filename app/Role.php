<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    |
    */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
