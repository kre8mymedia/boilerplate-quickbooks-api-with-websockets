<?php

namespace App\Http\Controllers;
// Laravel Packages
use Illuminate\Http\Request;
use Illuminate\Support\Str;
// Events
use App\Events\NewRole;
// Models
use App\Role;
use App\User;

class RolesController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_page()
    {
        return view('roles.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::with('user')->get();
        $users = User::all();
        // $roles = Role::all();
        return response()->json([
            'users' => $users,
            'roles' => $roles
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        $role = new Role;
        $role->uuid = Str::random(32);
        $role->user_id = $request->user_id;
        $role->name = $request->name;
        $role->save();

        broadcast(new NewRole($role))->toOthers();
        
        if (request()->is('api/*') == 1) {
            //API DATA
            return response()->json([
                'success' => "Role #{$role->id} created",
                'role' => $role
            ]);
        }

        return response()->json([
            'success' => "Role #{$role->id} created",
            'role' => $role
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::find($id);

        broadcast(new NewRole($role))->toOthers();

        $role->delete();

        return response()->json([
            'success' => "Role removed"
        ]);
    }
}
