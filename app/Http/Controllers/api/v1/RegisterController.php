<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Passport\Client as PassportClient;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
// use Illuminate\Foundation\Auth\RegistersUsers;

// Models
use App\User;
use App\Organization;
use App\Role;
// Helpers
use Throwable;

class RegisterController extends Controller
{
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }

    public function register(Request $request)
    {
        try {
            /**
             * Get a validator for an incoming registration request.
             *
             * @param  array  $request
             * @return \Illuminate\Contracts\Validation\Validator
             */

            $valid = validator($request->only('name', 'email', 'password'), [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8'],
            ]);

            if ($valid->fails()) {
                $jsonError = response()->json($valid->errors()->all(), 400);
                return Response::json($jsonError);
            }

            $data = request()->only('name', 'email', 'password');

            // // See if registered user organization exists
            // $organization = Organization::where('name', 'LIKE', '%'.$data['company'].'%')->first();
            // $org_didnt_exist = 0;
            // // If the organzation doesnt exist
            // if(!$organization) {
            //     // create new organization
            //     $organization = new Organization;
            //     $organization->serial = Str::random(32);
            //     $organization->name = $data['company'];
            //     $organization->save();

            //     $org_didnt_exist = 1;
            // }

            $user = User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
            ]);

            // if ($org_didnt_exist == 1) {
            //     // create SuperUser role for newuser
            //     $role = new Role;
            //     $role->user_id = $user->id;
            //     $role->organization_id = $organization->id;
            //     $role->name = 'SuperUser';
            //     $role->save();
            // } else {
            //     // If so create Viewer role for newuser
            //     $role = new Role;
            //     $role->user_id = $user->id;
            //     $role->organization_id = $organization->id;
            //     $role->name = 'Viewer';
            //     $role->save();
            // }

            // $organization = Organization::find($organization->id);
            // $organization->primary_user_id = $user->id;
            // $organization->save();

            if ( ! Hash::check($request->password, $user->password, [])) {
                throw new Throwable('Error in Login');
            }

            $tokenResult = $user->createToken('authToken')->plainTextToken;
            return response()->json([
                'status_code' => 200,
                'access_token' => $tokenResult,
                'token_type' => 'Bearer',
            ]);

        } catch (Throwable $error) {
            return response()->json([
                'status_code' => 500,
                'message' => 'Error in Login',
                'error' => $error,
            ]);
        }
    }
}
