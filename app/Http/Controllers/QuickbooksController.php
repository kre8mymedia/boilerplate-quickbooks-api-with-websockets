<?php

namespace App\Http\Controllers;

// Packages
use Illuminate\Http\Request;
use QuickBooksOnline\API\DataService\DataService;
// Models
use QuickBooksOnline\API\Facades\Account;
use QuickBooksOnline\API\Facades\Bill;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Estimate;
use QuickBooksOnline\API\Facades\Item;
use QuickBooksOnline\API\Facades\Vendor;
use App\QBToken;
// Helpers
use Illuminate\Support\Str;
use Throwable;

class QuickbooksController extends Controller
{
    private $clientId;
    private $clientSecret;
    private $redirectURI;

    public function __construct() 
    {
        $this->clientId = env('QUICKBOOKS_CLIENT_ID');
        $this->clientSecret = env('QUICKBOOKS_CLIENT_SECRET');
        $this->redirectURI = env('QUICKBOOKS_REDIRECT_URI');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private function getDataService()
    {
        $user = auth()->user();
        $token = QBToken::where('user_id', $user->id)->first();
        // Check if token exists, if not return null
        if (!$token) {
            return null;
        }
        // Prep Data Services
        $dataService = DataService::Configure([
            'auth_mode' => 'oauth2',
            'ClientID' => $this->clientId,
            'ClientSecret' => $this->clientSecret,
            'accessTokenKey' => $token->accessToken,
            'refreshTokenKey' => $token->refreshToken,
            'QBORealmID' => $token->realmId,
            'baseUrl' => "Development"
        ]);
        // Return if token existed
        return $dataService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getTokens(Request $request)
    {
        $dataService = DataService::Configure([
            'auth_mode' => 'oauth2',
            'ClientID' => $this->clientId,
            'ClientSecret' => $this->clientSecret,
            'RedirectURI' => $this->redirectURI,
            'scope' => "com.intuit.quickbooks.accounting",
            'baseUrl' => "Development"
        ]);

        $OAuth2LoginHelper = $dataService->getOAuth2LoginHelper();
        $accessTokenObj = $OAuth2LoginHelper->exchangeAuthorizationCodeForToken($request['code'], $request['realmId']);
        $accessTokenValue = $accessTokenObj->getAccessToken();
        $refreshTokenValue = $accessTokenObj->getRefreshToken();
        // See if existing token in db with this user_id
        $existing_token = QBToken::where('user_id', auth()->user()->id)->first();
        // if so update tokens
        if($existing_token) {
            $existing_token->user_id = auth()->user()->id;
            $existing_token->accessToken = $accessTokenValue;
            $existing_token->refreshToken = $refreshTokenValue;
            $existing_token->realmId = $request->realmId;
            $existing_token->save();
            $qb_token = $existing_token;
            $action = 'updated';
        } else {
            $qb_token = new QBToken;
            $qb_token->user_id = auth()->user()->id;
            $qb_token->accessToken = $accessTokenValue;
            $qb_token->refreshToken = $refreshTokenValue;
            $qb_token->realmId = $request->realmId;
            $qb_token->save();
            $action = 'added';
        }

        if (request()->is('api/*') == 1) {
            return response()->json([
                'success' => "Token {$action}",
                'qb_token' => $qb_token,
            ]);
        } else {
            return redirect('home')->with([
                'success' => "Token {$action}",
                'qb_token' => $qb_token,
            ]);
        }
    }

    /**
     * OAuth Login Link.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataService = DataService::Configure([
            'auth_mode' => 'oauth2',
            'ClientID' => $this->clientId,
            'ClientSecret' => $this->clientSecret,
            'RedirectURI' => $this->redirectURI,
            'scope' => "com.intuit.quickbooks.accounting",
            'baseUrl' => "Development"
        ]);
        $OAuth2LoginHelper = $dataService->getOAuth2LoginHelper();
        $authorizationCodeUrl = $OAuth2LoginHelper->getAuthorizationCodeURL();
        return redirect($authorizationCodeUrl);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCompanyPrefs()
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        return json_decode(json_encode($dataService->getCompanyPreferences()), true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateCompanyPrefs()
    {
        return "Coming Soon";
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        return json_decode(json_encode($dataService->getCompanyPreferences()), true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCompanyInfo()
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        return json_decode(json_encode($dataService->getCompanyInfo()), true);
    }

    /**-------------------------------------------------------------------------
     * QB Account Routes
     *--------------------------------------------------------------------------
     * @return \Illuminate\Http\Response
     * 
     */
    // List Accounts
    public function accounts()
    {
        try {
            $dataService = $this->getDataService();
            // Check if request is api or not
            if (request()->is('api/*') == 1) {
                if (!$dataService) {
                    // if api return a Oauth2 redirect route
                    return response()->json([
                        'error' => 'You have authorized your Quickbooks account',
                        'redirect' => url('/qb_login')
                    ]);
                }
            } else {
                // if app reoute redirect to OAuth2 route
                if (!$dataService) {
                    return redirect('/qb_login');
                }
            }
            $allAccounts = $dataService->FindAll('Account');

            // success response
            return response()->json([
                'count' => count($allAccounts),
                'accounts' => $allAccounts
            ]);

        } catch(Throwable $ex) {
            // format response
            $response = [
                'datetime' => date("F j, Y, g:i:s A" , time()),
                'error' => $ex->getMessage()
            ];
            // if the debug set to true include the stack trace
            if (env('APP_DEBUG')) {
                $response['stack'] = $ex->getTrace();
            }
            // check if request is from api
            if (request()->is('api/*') == 1) {
                // return error response
                return response()->json($response);
            } else {
                // return error response or error page
                return (env('APP_DEBUG')) ? response()->json($response) : abort(500);
            }
        }
    }

    /**
     * SHOW ACCOUNT
     */
    public function account_show($id)
    {
        try {
            $dataService = $this->getDataService();
            // Check if request is api or not
            if (request()->is('api/*') == 1) {
                if (!$dataService) {
                    // if api return a Oauth2 redirect route
                    return response()->json([
                        'error' => 'You have authorized your Quickbooks account',
                        'redirect' => url('/qb_login')
                    ]);
                }
            } else {
                // if app reoute redirect to OAuth2 route
                if (!$dataService) {
                    return redirect('/qb_login');
                }
            }
            $findAccount = $dataService->FindById('Account', $id);

            return response()->json([
                'account' => $findAccount
            ]);
        } catch(Throwable $ex) {
            // format response
            $response = [
                'datetime' => date("F j, Y, g:i:s A" , time()),
                'error' => $ex->getMessage()
            ];
            // if the debug set to true include the stack trace
            if (env('APP_DEBUG')) {
                $response['stack'] = $ex->getTrace();
            }
            // check if request is from api
            if (request()->is('api/*') == 1) {
                // return error response
                return response()->json($response);
            } else {
                // return error response or error page
                return (env('APP_DEBUG')) ? response()->json($response) : abort(500);
            }
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function account_page()
    {
        return view('accounts.create');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function account_create(Request $request)
    {
        try {
            $dataService = $this->getDataService();
            // Check if request is api or not
            if (request()->is('api/*') == 1) {
                if (!$dataService) {
                    // if api return a Oauth2 redirect route
                    return response()->json([
                        'error' => 'You have authorized your Quickbooks account',
                        'redirect' => url('/qb_login')
                    ]);
                }
            } else {
                // if app reoute redirect to OAuth2 route
                if (!$dataService) {
                    return redirect('/qb_login');
                }
            }
            $dataService->setLogLocation("./logs");
            $dataService->throwExceptionOnError(true);

            $resourceObj = Account::create([
                'Name' => $request->Name,
                'Description' => $request->Description,
                'AccountSubType' => $request->AccountSubType,
                'AcctNum' => rand(100000, 9999999)
            ]);

            $resultingObj = $dataService->Add($resourceObj);

            return response()->json([
                'success' => "Account #{$resultingObj->Id} created",
                'account' => $resultingObj
            ]);
            
        } catch(Throwable $ex) {
            // format response
            $response = [
                'datetime' => date("F j, Y, g:i:s A" , time()),
                'error' => $ex->getMessage()
            ];
            // if the debug set to true include the stack trace
            if (env('APP_DEBUG')) {
                $response['stack'] = $ex->getTrace();
            }
            // check if request is from api
            if (request()->is('api/*') == 1) {
                // return error response
                return response()->json($response);
            } else {
                // return error response or error page
                return (env('APP_DEBUG')) ? response()->json($response) : abort(500);
            }
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function account_update(Request $request, $id)
    {
        try {
            $dataService = $this->getDataService();
            // Check if request is api or not
            if (request()->is('api/*') == 1) {
                if (!$dataService) {
                    // if api return a Oauth2 redirect route
                    return response()->json([
                        'error' => 'You have authorized your Quickbooks account',
                        'redirect' => url('/qb_login')
                    ]);
                }
            } else {
                // if app reoute redirect to OAuth2 route
                if (!$dataService) {
                    return redirect('/qb_login');
                }
            }
            $dataService->setLogLocation("./logs");
            $dataService->throwExceptionOnError(true);
            // Find Resource
            $account = $dataService->FindbyId('account', $id);
            // Update Properties
            $resourceObj = Account::update($account, [
                'Active' => $request->Active,
                'Name' => $request->Name,
                'Description' => $request->Description,
                'AccountSubType' => $request->AccountSubType
            ]);
            // Send off update
            $resultinObj = $dataService->Update($resourceObj);
            // Return response to user
            return response()->json([
                'success' => "Account #{$resultinObj->Id} updated",
                'account' => $resultinObj
            ]);
        } catch(Throwable $ex) {
            // format response
            $response = [
                'datetime' => date("F j, Y, g:i:s A" , time()),
                'error' => $ex->getMessage()
            ];
            // if the debug set to true include the stack trace
            if (env('APP_DEBUG')) {
                $response['stack'] = $ex->getTrace();
            }
            // check if request is from api
            if (request()->is('api/*') == 1) {
                // return error response
                return response()->json($response);
            } else {
                // return error response or error page
                return (env('APP_DEBUG')) ? response()->json($response) : abort(500);
            }
        }
    }

    /**
     * *** NOT SUPPORTED ***
     *
     * @return \Illuminate\Http\Response
     */
    public function account_delete($id)
    {
        try {
            $dataService = $this->getDataService();
            // Check if request is api or not
            if (request()->is('api/*') == 1) {
                if (!$dataService) {
                    // if api return a Oauth2 redirect route
                    return response()->json([
                        'error' => 'You have authorized your Quickbooks account',
                        'redirect' => url('/qb_login')
                    ]);
                }
            } else {
                // if app reoute redirect to OAuth2 route
                if (!$dataService) {
                    return redirect('/qb_login');
                }
            }
            $dataService->setLogLocation("./logs");
            $dataService->throwExceptionOnError(true);

            $resourceObj = $dataService->FindbyId('account', $id);
            $resultingObj = $dataService->Delete($resourceObj);

            return response()->json([
                'success' => "Account removed",
                'resultingObj' => $resultingObj
            ]);
        } catch(Throwable $ex) {
            // format response
            $response = [
                'datetime' => date("F j, Y, g:i:s A" , time()),
                'error' => $ex->getMessage()
            ];
            // if the debug set to true include the stack trace
            if (env('APP_DEBUG')) {
                $response['stack'] = $ex->getTrace();
            }
            // check if request is from api
            if (request()->is('api/*') == 1) {
                // return error response
                return response()->json($response);
            } else {
                // return error response or error page
                return (env('APP_DEBUG')) ? response()->json($response) : abort(500);
            }
        }
    }

    /** -------------------------------------------------------------------------
     * QB Bill Routes
     * --------------------------------------------------------------------------
     * 
     * @return \Illuminate\Http\Response
     * 
     */
    public function bills()
    {
        try {
            $dataService = $this->getDataService();
            // Check if request is api or not
            if (request()->is('api/*') == 1) {
                if (!$dataService) {
                    // if api return a Oauth2 redirect route
                    return response()->json([
                        'error' => 'You have authorized your Quickbooks account',
                        'redirect' => url('/qb_login')
                    ]);
                }
            } else {
                // if app reoute redirect to OAuth2 route
                if (!$dataService) {
                    return redirect('/qb_login');
                }
            }
            $allBills = $dataService->FindAll('Bill');
            return response()->json([
                'count' => count($allBills),
                'bills' => $allBills
            ]);
        } catch(Throwable $ex) {
            // format response
            $response = [
                'datetime' => date("F j, Y, g:i:s A" , time()),
                'error' => $ex->getMessage()
            ];
            // if the debug set to true include the stack trace
            if (env('APP_DEBUG')) {
                $response['stack'] = $ex->getTrace();
            }
            // check if request is from api
            if (request()->is('api/*') == 1) {
                // return error response
                return response()->json($response);
            } else {
                // return error response or error page
                return (env('APP_DEBUG')) ? response()->json($response) : abort(500);
            }
        }
    }

    /**
     * SHOW Bill
     */
    public function bill_show($id)
    {
       try {
            $dataService = $this->getDataService();
            // Check if request is api or not
            if (request()->is('api/*') == 1) {
                if (!$dataService) {
                    // if api return a Oauth2 redirect route
                    return response()->json([
                        'error' => 'You have authorized your Quickbooks account',
                        'redirect' => url('/qb_login')
                    ]);
                }
            } else {
                // if app reoute redirect to OAuth2 route
                if (!$dataService) {
                    return redirect('/qb_login');
                }
            }
            $findBill = $dataService->FindById('Bill', $id);
            return response()->json([
                'bill' => $findBill
            ]);
        } catch(Throwable $ex) {
            // format response
            $response = [
                'datetime' => date("F j, Y, g:i:s A" , time()),
                'error' => $ex->getMessage()
            ];
            // if the debug set to true include the stack trace
            if (env('APP_DEBUG')) {
                $response['stack'] = $ex->getTrace();
            }
            // check if request is from api
            if (request()->is('api/*') == 1) {
                // return error response
                return response()->json($response);
            } else {
                // return error response or error page
                return (env('APP_DEBUG')) ? response()->json($response) : abort(500);
            }
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function bill_page()
    {
        return view('bills.create');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function bill_create(Request $request)
    {
        try {
            $dataService = $this->getDataService();
            // Check if request is api or not
            if (request()->is('api/*') == 1) {
                if (!$dataService) {
                    // if api return a Oauth2 redirect route
                    return response()->json([
                        'error' => 'You have authorized your Quickbooks account',
                        'redirect' => url('/qb_login')
                    ]);
                }
            } else {
                // if app reoute redirect to OAuth2 route
                if (!$dataService) {
                    return redirect('/qb_login');
                }
            }
            $dataService->setLogLocation("./logs");
            $dataService->throwExceptionOnError(true);

            $line_items = [];
            foreach( $request->LineItem as $LineItem ) {
                // Check what the line item detail type is
                if ($LineItem['DetailType'] == 'AccountBasedExpenseLineDetail') {
                    // Format object for Account Based Expense
                    $line_item = [
                        'DetailType' => $LineItem['DetailType'], // String | REQUIRED
                        'Amount' => (double) $LineItem['Amount'], // Double | REQUIRED
                        'Description'=> $LineItem['Description'], // String | default: null
                        $LineItem['DetailType'] => [ // Object
                            'CustomerRef' => [
                                'value' => $LineItem['CustomerRef']['value'], // String | default: null
                            ],
                            'AccountRef' => [
                                'value' => $LineItem['AccountRef']['value'] // String | REQUIRED
                            ],
                            'BillableStatus' => $LineItem['BillableStatus'], // ENUM | default: NotBillable,
                            'MarkupInfo' => [
                                'Percent' => $LineItem['MarkupInfo']['Percent']
                            ]
                        ]
                    ];
                } else {
                    // Format object for Item Based Expense
                    $line_item = [
                        'DetailType' => $LineItem['DetailType'], // String | REQUIRED
                        'Amount' => $LineItem['Qty'] * $LineItem['UnitPrice'],
                        'Description'=> $LineItem['Description'], // String | default: null
                        $LineItem['DetailType'] => [ // Object
                            'ItemRef' => [
                                'value' => $LineItem['ItemRef']['value']
                            ],
                            'Qty' => $LineItem['Qty'], // Decimal
                            'UnitPrice' => $LineItem['UnitPrice'],
                            'CustomerRef' => [
                                'value' => $LineItem['CustomerRef']['value'], // String | default: null
                            ],
                            'BillableStatus' => $LineItem['BillableStatus'], // ENUM | default: NotBillable,
                            'MarkupInfo' => [
                                'Percent' => $LineItem['MarkupInfo']['Percent']
                            ]
                        ]
                    ];
                }
                // Push LineItem onto array
                array_push($line_items, $line_item);
            }
            
            $resourceObj = Bill::create([
                'Line' => $line_items, // Object | REQUIRED
                'VendorRef' => [
                    'value' => $request->VendorRef['value']
                ],
                'SalesTermRef' => [
                    'value' => $request->SalesTermRef['value'] // String | default: null
                ], 
                'DueDate' => $request->DueDate, // Date | default: Today
                'TxnDate' => $request->TxnDate,
                'Memo' => $request->Memo, // String | default: null
                'DocNumber' => time(), // String | default: null
                'PrivateNote' => $request->PrivateNote, // String | default: null
            ]);

            $resultingObj = $dataService->Add($resourceObj);
            return response()->json([
                'success' => "Bill #{$resultingObj->Id} created",
                'bill' => $resultingObj
            ]);

        } catch(Throwable $ex) {
            // format response
            $response = [
                'datetime' => date("F j, Y, g:i:s A" , time()),
                'error' => $ex->getMessage()
            ];
            // if the debug set to true include the stack trace
            if (env('APP_DEBUG')) {
                $response['stack'] = $ex->getTrace();
            }
            // check if request is from api
            if (request()->is('api/*') == 1) {
                // return error response
                return response()->json($response);
            } else {
                // return error response or error page
                return (env('APP_DEBUG')) ? response()->json($response) : abort(500);
            }
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function bill_edit_page($id)
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        $dataService->setLogLocation("./logs");
        $dataService->throwExceptionOnError(true);

        $bill = $dataService->FindbyId('bill', $id);
        
        return view('bills.edit')->with([
            'bill' => $bill
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function bill_update(Request $request, $id)
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        $dataService->setLogLocation("./logs");
        $dataService->throwExceptionOnError(true);

        $bill = $dataService->FindbyId('bill', $id);

        $line_items = [];
        foreach( $request->LineItem as $LineItem ) {
            // Check what the line item detail type is
            if ($LineItem['DetailType'] == 'AccountBasedExpenseLineDetail') {
                // Format object for Account Based Expense
                $line_item = [
                    'Id'=> $LineItem['Id'],
                    'DetailType' => $LineItem['DetailType'], // String | REQUIRED
                    'Amount' => (double) $LineItem['Amount'], // Double | REQUIRED
                    'Description'=> $LineItem['Description'], // String | default: null
                    $LineItem['DetailType'] => [ // Object
                        'CustomerRef' => [
                            'value' => $LineItem['CustomerRef']['value'], // String | default: null
                        ],
                        'AccountRef' => [
                            'value' => $LineItem['AccountRef']['value'] // String | REQUIRED
                        ],
                        'BillableStatus' => $LineItem['BillableStatus'], // ENUM | default: NotBillable,
                        'MarkupInfo' => [
                            'Percent' => $LineItem['MarkupInfo']['Percent']
                        ]
                    ]
                ];
            } else {
                // Format object for Item Based Expense
                $line_item = [
                    'Id'=> $LineItem['Id'],
                    'DetailType' => $LineItem['DetailType'], // String | REQUIRED
                    'Amount' => $LineItem['Qty'] * $LineItem['UnitPrice'],
                    'Description'=> $LineItem['Description'], // String | default: null
                    $LineItem['DetailType'] => [ // Object
                        'ItemRef' => [
                            'value' => $LineItem['ItemRef']['value']
                        ],
                        'Qty' => $LineItem['Qty'], // Decimal
                        'UnitPrice' => $LineItem['UnitPrice'],
                        'CustomerRef' => [
                            'value' => $LineItem['CustomerRef']['value'], // String | default: null
                        ],
                        'BillableStatus' => $LineItem['BillableStatus'], // ENUM | default: NotBillable,
                        'MarkupInfo' => [
                            'Percent' => $LineItem['MarkupInfo']['Percent']
                        ]
                    ]
                ];
            }
            // Push LineItem onto array
            array_push($line_items, $line_item);
        }
        
        $resourceObj = Bill::update($bill, [
            'Line' => $line_items, // Object | REQUIRED
            'VendorRef' => [
                'value' => $request->VendorRef['value']
            ],
            'SalesTermRef' => [
                'value' => $request->SalesTermRef['value'] // String | default: null
            ], 
            'DueDate' => $request->DueDate, // Date | default: Today
            'TxnDate' => $request->TxnDate,
            'Memo' => $request->Memo, // String | default: null
            'PrivateNote' => $request->PrivateNote, // String | default: null
        ]);

        $resultingObj = $dataService->Update($resourceObj);

        return response()->json([
            'success' => "Bill {$resultingObj->Id} updated",
            'bill' => $resultingObj
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function bill_delete($id)
    {
        try {
            $dataService = $this->getDataService();
            // Check if request is api or not
            if (request()->is('api/*') == 1) {
                if (!$dataService) {
                    // if api return a Oauth2 redirect route
                    return response()->json([
                        'error' => 'You have authorized your Quickbooks account',
                        'redirect' => url('/qb_login')
                    ]);
                }
            } else {
                // if app reoute redirect to OAuth2 route
                if (!$dataService) {
                    return redirect('/qb_login');
                }
            }
            $dataService->setLogLocation("./logs");
            $dataService->throwExceptionOnError(true);

            $resourceObj = $dataService->FindbyId('bill', $id);
            $resultingObj = $dataService->Delete($resourceObj);

            return response()->json([
                'success' => "Bill #{$resultingObj->Id} removed"
            ]);
        } catch(Throwable $ex) {
            // format response
            $response = [
                'datetime' => date("F j, Y, g:i:s A" , time()),
                'error' => $ex->getMessage()
            ];
            // if the debug set to true include the stack trace
            if (env('APP_DEBUG')) {
                $response['stack'] = $ex->getTrace();
            }
            // check if request is from api
            if (request()->is('api/*') == 1) {
                // return error response
                return response()->json($response);
            } else {
                // return error response or error page
                return (env('APP_DEBUG')) ? response()->json($response) : abort(500);
            }
        }
    }

    /** -------------------------------------------------------------------------
     * QB BillPayment Routes
     * --------------------------------------------------------------------------
     * 
     * @return \Illuminate\Http\Response
     * 
     */
    public function bill_payments()
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        $allBillPayments = $dataService->FindAll('BillPayment');

        return response()->json([
            'count' => count($allBillPayments),
            'bill_payments' => $allBillPayments
        ]);
    }

    /**
     * SHOW BillPayment
     */
    public function bill_payment_show($id)
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        $findBillPayment = $dataService->FindById('BillPayment', $id);

        return response()->json([
            'bill_payment' => $findBillPayment
        ]);
    }

    /**-------------------------------------------------------------------------
     * QB Customer Routes
     *--------------------------------------------------------------------------
     * @return \Illuminate\Http\Response
     * 
     */
    public function customers()
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        $allCustomers = $dataService->FindAll('Customer');

        return response()->json([
            'count' => count($allCustomers),
            'customers' => $allCustomers
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function customer_page()
    {
        return view('customers.create');
    }

    /**
     * SHOW Customer
     */
    public function customer_show($id)
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        $findCustomer = $dataService->FindById('Customer', $id);

        return response()->json([
            'customer' => $findCustomer
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function customer_create(Request $request)
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        $dataService->setLogLocation("./logs");
        $dataService->throwExceptionOnError(true);

        // Add a new Customer
        $customer = Customer::create([
            'GivenName' => $request->GivenName,
            'MiddleName' => $request->MiddleName,
            'FamilyName' => $request->FamilyName,
            'Suffix' => $request->Suffix,
            'PrintOnCheckName' => $request->PrintOnCheckName,
            'Title' => $request->Title,
            'CompanyName' => $request->CompanyName,
            'WebAddr' => [
                'URI' => $request->WebAddr
            ],
            'PrimaryEmailAddr' => [
                'Address' => $request->PrimaryEmailAddr
            ],
            'PrimaryPhone' => [
                "FreeFormNumber" => $request->PrimaryPhone,
                'DeviceType' => $request->DeviceType
            ],
            'Mobile' => [
                "FreeFormNumber" => $request->Mobile
            ],
            'Fax' => [
                'FreeFormNumber' => $request->Fax
            ],
            'BillAddr' => [
                'Line1' => $request->BillAddr_Line1,
                'Line2' => $request->BillAddr_Line2,
                'Line3' => $request->BillAddr_Line3,
                'Line4' => $request->BillAddr_Line4,
                'Line5' => $request->BillAddr_Line5,
                'City' => $request->BillAddr_City,
                'CountryCode' => $request->BillAddr_CountryCode,
                'Country' => $request->BillAddr_Country,
                'County' => $request->BillAddr_County,
                'CountrySubDivisionCode' => $request->BillAddr_CountrySubDivisionCode,
                'PostalCode' => $request->BillAddr_PostalCode,
                'PostalCodeSuffix' => $request->BillAddr_PostalCodeSuffix,
                'Lat' => $request->BillAddr_Lat,
                'Long' => $request->BillAddr_Long,
                'Tag' => $request->BillAddr_Tag,
                'Note' => $request->BillAddr_Note
            ],
            'ShipAddr' => [
                'Line1' => $request->ShipAddr_Line1,
                'Line2' => $request->ShipAddr_Line2,
                'Line3' => $request->ShipAddr_Line3,
                'Line4' => $request->ShipAddr_Line4,
                'Line5' => $request->ShipAddr_Line5,
                'City' => $request->ShipAddr_City,
                'CountryCode' => $request->ShipAddr_CountryCode,
                'Country' => $request->ShipAddr_Country,
                'County' => $request->ShipAddr_County,
                'CountrySubDivisionCode' => $request->ShipAddr_CountrySubDivisionCode,
                'PostalCode' => $request->ShipAddr_PostalCode,
                'PostalCodeSuffix' => $request->ShipAddr_PostalCodeSuffix,
                'Lat' => $request->ShipAddr_Lat,
                'Long' => $request->ShipAddr_Long,
                'Tag' => $request->ShipAddr_Tag,
                'Note' => $request->ShipAddr_Note
            ],
            'PreferredDeliveryMethod' => $request->PreferredDeliveryMethod,
            'TaxExemptionReasonId' => $request->TaxExemptionReasonId,
            'Notes' => $request->Notes,
            'Taxable' => $request->Taxable,
            'Active' => $request->Active,
            'IsProject' => $request->IsProject,

        ]);

        $resultingObj = $dataService->Add($customer);

        return response()->json([
            'success' => "Customer #{$resultingObj->Id} created",
            'customer' => $resultingObj
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function customer_update(Request $request, $id)
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        $dataService->setLogLocation("./logs");
        $dataService->throwExceptionOnError(true);

        $customer = $dataService->FindbyId('customer', $id);

        // Add a new Customer
        $resourceObj = Customer::update($customer, [
            'GivenName' => $request->GivenName,
            'MiddleName' => $request->MiddleName,
            'FamilyName' => $request->FamilyName,
            'Suffix' => $request->Suffix,
            'PrintOnCheckName' => $request->PrintOnCheckName,
            'Title' => $request->Title,
            'CompanyName' => $request->CompanyName,
            'WebAddr' => [
                'URI' => $request->WebAddr
            ],
            'PrimaryEmailAddr' => [
                'Address' => $request->PrimaryEmailAddr
            ],
            'PrimaryPhone' => [
                "FreeFormNumber" => $request->PrimaryPhone,
                'DeviceType' => $request->DeviceType
            ],
            'Mobile' => [
                "FreeFormNumber" => $request->Mobile
            ],
            'Fax' => [
                'FreeFormNumber' => $request->Fax
            ],
            'BillAddr' => [
                'Line1' => $request->BillAddr_Line1,
                'Line2' => $request->BillAddr_Line2,
                'Line3' => $request->BillAddr_Line3,
                'Line4' => $request->BillAddr_Line4,
                'Line5' => $request->BillAddr_Line5,
                'City' => $request->BillAddr_City,
                'CountryCode' => $request->BillAddr_CountryCode,
                'Country' => $request->BillAddr_Country,
                'County' => $request->BillAddr_County,
                'CountrySubDivisionCode' => $request->BillAddr_CountrySubDivisionCode,
                'PostalCode' => $request->BillAddr_PostalCode,
                'PostalCodeSuffix' => $request->BillAddr_PostalCodeSuffix,
                'Lat' => $request->BillAddr_Lat,
                'Long' => $request->BillAddr_Long,
                'Tag' => $request->BillAddr_Tag,
                'Note' => $request->BillAddr_Note
            ],
            'ShipAddr' => [
                'Line1' => $request->ShipAddr_Line1,
                'Line2' => $request->ShipAddr_Line2,
                'Line3' => $request->ShipAddr_Line3,
                'Line4' => $request->ShipAddr_Line4,
                'Line5' => $request->ShipAddr_Line5,
                'City' => $request->ShipAddr_City,
                'CountryCode' => $request->ShipAddr_CountryCode,
                'Country' => $request->ShipAddr_Country,
                'County' => $request->ShipAddr_County,
                'CountrySubDivisionCode' => $request->ShipAddr_CountrySubDivisionCode,
                'PostalCode' => $request->ShipAddr_PostalCode,
                'PostalCodeSuffix' => $request->ShipAddr_PostalCodeSuffix,
                'Lat' => $request->ShipAddr_Lat,
                'Long' => $request->ShipAddr_Long,
                'Tag' => $request->ShipAddr_Tag,
                'Note' => $request->ShipAddr_Note
            ],
            'PreferredDeliveryMethod' => $request->PreferredDeliveryMethod,
            'TaxExemptionReasonId' => $request->TaxExemptionReasonId,
            'Notes' => $request->Notes,
            'Taxable' => $request->Taxable,
            'Active' => $request->Active,
            'IsProject' => $request->IsProject,
        ]);

        $resultingObj = $dataService->Update($resourceObj);

        return response()->json([
            'success' => "Customer #{$resultingObj->Id} updated",
            'customer' => $resultingObj
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function customer_delete($id)
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        $dataService->setLogLocation("./logs");
        $dataService->throwExceptionOnError(true);

        $vendor = $dataService->FindbyId('customer', $id);

        $resultingObj = $dataService->Delete($vendor);

        return response()->json([
            'success' => "Customer #{$id} removed",
            'customer' => $resultingObj
        ]);
    }

    /** -------------------------------------------------------------------------
     * QB Estimate Routes
     * --------------------------------------------------------------------------
     * 
     * @return \Illuminate\Http\Response
     * 
     */
    public function estimates()
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        $allEstimates = $dataService->FindAll('Estimate');

        return response()->json([
            'count' => count($allEstimates),
            'estimates' => $allEstimates
        ]);
    }

    /**
     * SHOW Estimate
     */
    public function estimate_show($id)
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        $findEstimate = $dataService->FindById('Estimate', $id);

        return response()->json([
            'estimate' => $findEstimate
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function estimate_page()
    {
        return view('estimates.create');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function estimate_create(Request $request)
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        $dataService->setLogLocation("~/logs");
        $dataService->throwExceptionOnError(true);

        // Add a new Customer
        $estimate = Estimate::create([
            
        ]);

        $resultingObj = $dataService->Add($estimate);

        return response()->json([
            'success' => "Estimate #{$resultingObj->Id} created",
            'estimate' => $resultingObj
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function estimate_update(Request $request, $id)
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        $dataService->setLogLocation("~/logs");
        $dataService->throwExceptionOnError(true);

        $estimate = $dataService->FindbyId('estimate', $id);

        // Update a vendor
        $resourceObj = Vendor::update($estimate, [
            
        ]);

        $resultingObj = $dataService->Update($resourceObj);

        return response()->json([
            'success' => "Estimate #{$resultingObj->Id} updated",
            'estimate' => $resultingObj
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function estimate_delete($id)
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        $dataService->setLogLocation("./logs");
        $dataService->throwExceptionOnError(true);

        $estimate = $dataService->FindbyId('estimate', $id);

        $resultingObj = $dataService->Delete($estimate);

        return response()->json([
            'success' => "Estimate #{$id} removed",
            'estimate' => $resultingObj
        ]);
    }

    /** -------------------------------------------------------------------------
     * QB Invoice Routes
     * --------------------------------------------------------------------------
     * 
     * @return \Illuminate\Http\Response
     * 
     */
    public function invoices()
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        $allInvoices = $dataService->FindAll('Invoice');

        return response()->json([
            'count' => count($allInvoices),
            'invoices' => $allInvoices
        ]);
    }

    /**
     * SHOW Invoice
     */
    public function invoice_show($id)
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        $findInvoice = $dataService->FindById('Invoice', $id);

        return response()->json([
            'invoice' => $findInvoice
        ]);
    }

    /** -------------------------------------------------------------------------
     * QB Item Routes
     * --------------------------------------------------------------------------
     * 
     * @return \Illuminate\Http\Response
     * 
     */
    public function items()
    {
        try {
            $dataService = $this->getDataService();
            // Check if request is api or not
            if (request()->is('api/*') == 1) {
                if (!$dataService) {
                    // if api return a Oauth2 redirect route
                    return response()->json([
                        'error' => 'You have authorized your Quickbooks account',
                        'redirect' => url('/qb_login')
                    ]);
                }
            } else {
                // if app reoute redirect to OAuth2 route
                if (!$dataService) {
                    return redirect('/qb_login');
                }
            }
            $allItems = $dataService->FindAll('Item');

            return response()->json([
                'count' => count($allItems),
                'items' => $allItems
            ]);

        } catch(Exception $ex){ 
            if (request()->is('api/*') == 1) {
                //API DATA
                return response()->json([
                    'datetime' => date("F j, Y, g:i:s A" , time()),
                    'error' => $ex,                    
                ]);
            } else {
                //SPA DATA
                return response()->json([
                    'datetime' => date("F j, Y, g:i:s A" , time()),
                    'error' => $ex,                    
                ]);
            }
        }
    }

    /**
     * SHOW Estimate
     */
    public function item_show($id)
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        $findItem = $dataService->FindById('Item', $id);

        return response()->json([
            'item' => $findItem
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function item_page()
    {
        return view('items.create');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function item_create(Request $request)
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        $dataService->setLogLocation("~/logs");
        $dataService->throwExceptionOnError(true);

        if($request->QtyOnHand > 0) {
            $track_qty_on_hand = true;
        } else {
            $track_qty_on_hand = false;
        }

        // Add a new Customer
        $item = Item::create([
            'Name' => $request->Name,
            'QtyOnHand' => $request->QtyOnHand,
            'TrackQtyOnHand' => $track_qty_on_hand,
            'InvStartDate' => date('Y-m-d', time()),
            'Description' => $request->Description,
            'PurchaseCost' => (float) $request->PurchaseCost,
            'UnitPrice' => $request->UnitPrice,
            'Taxable' => $request->Taxable,
            'Type' => $request->Type,
            'Descripition' => $request->Description,
            'Sku' => (string) Str::uuid(),
            'IncomeAccountRef' => [
                'value' => $request->IncomeAccountId
            ],
            'AssetAccountRef' => [
                'value' => $request->AssetAccountId
            ],
            'ExpenseAccountRef' => [
                'value' => $request->ExpenseAccountId
            ]
        ]);

        $resultingObj = $dataService->Add($item);

        return response()->json([
            'success' => "Item #{$resultingObj->Id} created",
            'item' => $resultingObj
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function item_update(Request $request, $id)
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        $dataService->setLogLocation("~/logs");
        $dataService->throwExceptionOnError(true);

        $item = $dataService->FindbyId('item', $id);

        // Update a vendor
        $resourceObj = Item::update($item, [
            
        ]);

        $resultingObj = $dataService->Update($resourceObj);

        return response()->json([
            'success' => "Item #{$resultingObj->Id} updated",
            'item' => $resultingObj
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function item_delete($id)
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        $dataService->setLogLocation("./logs");
        $dataService->throwExceptionOnError(true);

        $item = $dataService->FindbyId('item', $id);

        $resultingObj = $dataService->Delete($item);

        return response()->json([
            'success' => "Item #{$id} removed",
            'item' => $resultingObj
        ]);
    }

    /** -------------------------------------------------------------------------
     * QB Journal Entry Routes
     * --------------------------------------------------------------------------
     * 
     * @return \Illuminate\Http\Response
     * 
     */
    public function journal_entries()
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        $allJournalEntries = $dataService->FindAll('JournalEntry');

        return response()->json([
            'count' => count($allJournalEntries),
            'journal_entries' => $allJournalEntries
        ]);
    }

    /**
     * SHOW Estimate
     */
    public function journal_entry_show($id)
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        $findJournalEntry = $dataService->FindById('JournalEntry', $id);

        return response()->json([
            'journal_entry' => $findJournalEntry
        ]);
    }

    /** -------------------------------------------------------------------------
     * QB Payment Routes
     * --------------------------------------------------------------------------
     * 
     * @return \Illuminate\Http\Response
     * 
     */
    public function payments()
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        $allPayments = $dataService->FindAll('Payment');

        return response()->json([
            'count' => count($allPayments),
            'payments' => $allPayments
        ]);
    }

    /**
     * SHOW Payment
     */
    public function payment_show($id)
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        $findPayment = $dataService->FindById('Payment', $id);

        return response()->json([
            'payment' => $findPayment
        ]);
    }

    /** -------------------------------------------------------------------------
     * QB Report Routes
     * --------------------------------------------------------------------------
     * 
     * @return \Illuminate\Http\Response
     * 
     */
    public function reports()
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        $allReports = $dataService->FindAll('Report');

        if ($allReports) {
            return response()->json([
                'count' => count($allReports),
                'reports' => $allReports
            ]);
        } else {
            return response()->json([
                'reports' => $allReports
            ]);
        }
    }

    // /**
    //  * SHOW Payment
    //  */
    // public function report_show($id)
    // {
    //     $dataService = $this->getDataService();

    //     $findReport = $dataService->FindById('Report', $id);

    //     if ($findReport) {
    //         return response()->json([
    //             'count' => count($findReport),
    //             'report' => $findReport
    //         ]);
    //     } else {
    //         return response()->json([
    //             'report' => $findReport
    //         ]);
    //     }
    // }

    /** -------------------------------------------------------------------------
     * QB Report Routes
     * --------------------------------------------------------------------------
     * 
     * @return \Illuminate\Http\Response
     * 
     */
    public function time_activities()
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        $allTimeActivities = $dataService->FindAll('TimeActivity');

        return response()->json([
            'count' => count($allTimeActivities),
            'time_activities' => $allTimeActivities
        ]);
    }

    /**
     * SHOW TimeActivity
     */
    public function time_activity_show($id)
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        $findTimeActivity = $dataService->FindById('TimeActivity', $id);

        return response()->json([
            'time_activity' => $findTimeActivity
        ]);
    }

    /** -------------------------------------------------------------------------
     * QB Vendor Routes
     * --------------------------------------------------------------------------
     * 
     * @return \Illuminate\Http\Response
     * 
     */
    public function vendors()
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        $allVendors = $dataService->FindAll('Vendor');

        return response()->json([
            'count' => count($allVendors),
            'vendors' => $allVendors
        ]);
    }

    /**
     * SHOW Vendor
     */
    public function vendor_show($id)
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        $findVendor = $dataService->FindById('Vendor', $id);

        return response()->json([
            'vendor' => $findVendor
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function vendor_page()
    {
        return view('vendors.create');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function vendor_create(Request $request)
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        $dataService->setLogLocation("~/logs");
        $dataService->throwExceptionOnError(true);

        // Add a new Customer
        $vendor = Vendor::create([
            'GivenName' => $request->GivenName,
            'MiddleName' => $request->MiddleName,
            'FamilyName' => $request->FamilyName,
            'Suffix' => $request->Suffix,
            'PrintOnCheckName' => $request->PrintOnCheckName,
            'Title' => $request->Title,
            'CompanyName' => $request->CompanyName,
            'WebAddr' => [
                'URI' => $request->WebAddr
            ],
            'PrimaryEmailAddr' => [
                'Address' => $request->PrimaryEmailAddr
            ],
            'PrimaryPhone' => [
                "FreeFormNumber" => $request->PrimaryPhone,
                'DeviceType' => $request->DeviceType
            ],
            'Mobile' => [
                "FreeFormNumber" => $request->Mobile
            ],
            'Fax' => [
                'FreeFormNumber' => $request->Fax
            ],
            'BillAddr' => [
                'Line1' => $request->BillAddr_Line1,
                'Line2' => $request->BillAddr_Line2,
                'Line3' => $request->BillAddr_Line3,
                'Line4' => $request->BillAddr_Line4,
                'Line5' => $request->BillAddr_Line5,
                'City' => $request->BillAddr_City,
                'CountryCode' => $request->BillAddr_CountryCode,
                'Country' => $request->BillAddr_Country,
                'County' => $request->BillAddr_County,
                'CountrySubDivisionCode' => $request->BillAddr_CountrySubDivisionCode,
                'PostalCode' => $request->BillAddr_PostalCode,
                'PostalCodeSuffix' => $request->BillAddr_PostalCodeSuffix,
                'Lat' => $request->BillAddr_Lat,
                'Long' => $request->BillAddr_Long,
                'Tag' => $request->BillAddr_Tag,
                'Note' => $request->BillAddr_Note
            ],
            'Notes' => $request->Notes,
        ]);

        $resultingObj = $dataService->Add($vendor);

        return response()->json([
            'success' => "Vendor #{$resultingObj->Id} created",
            'vendor' => $resultingObj
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function vendor_update(Request $request, $id)
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        $dataService->setLogLocation("~/logs");
        $dataService->throwExceptionOnError(true);

        $vendor = $dataService->FindbyId('vendor', $id);

        // Update a vendor
        $resourceObj = Vendor::update($vendor, [
            'GivenName' => $request->GivenName,
            'MiddleName' => $request->MiddleName,
            'FamilyName' => $request->FamilyName,
            'Suffix' => $request->Suffix,
            'PrintOnCheckName' => $request->PrintOnCheckName,
            'Title' => $request->Title,
            'CompanyName' => $request->CompanyName,
            'WebAddr' => [
                'URI' => $request->WebAddr
            ],
            'PrimaryEmailAddr' => [
                'Address' => $request->PrimaryEmailAddr
            ],
            'PrimaryPhone' => [
                "FreeFormNumber" => $request->PrimaryPhone,
                'DeviceType' => $request->DeviceType
            ],
            'Mobile' => [
                "FreeFormNumber" => $request->Mobile
            ],
            'Fax' => [
                'FreeFormNumber' => $request->Fax
            ],
            'BillAddr' => [
                'Line1' => $request->BillAddr_Line1,
                'Line2' => $request->BillAddr_Line2,
                'Line3' => $request->BillAddr_Line3,
                'Line4' => $request->BillAddr_Line4,
                'Line5' => $request->BillAddr_Line5,
                'City' => $request->BillAddr_City,
                'CountryCode' => $request->BillAddr_CountryCode,
                'Country' => $request->BillAddr_Country,
                'County' => $request->BillAddr_County,
                'CountrySubDivisionCode' => $request->BillAddr_CountrySubDivisionCode,
                'PostalCode' => $request->BillAddr_PostalCode,
                'PostalCodeSuffix' => $request->BillAddr_PostalCodeSuffix,
                'Lat' => $request->BillAddr_Lat,
                'Long' => $request->BillAddr_Long,
                'Tag' => $request->BillAddr_Tag,
                'Note' => $request->BillAddr_Note
            ],
            'ShipAddr' => [
                'Line1' => $request->ShipAddr_Line1,
                'Line2' => $request->ShipAddr_Line2,
                'Line3' => $request->ShipAddr_Line3,
                'Line4' => $request->ShipAddr_Line4,
                'Line5' => $request->ShipAddr_Line5,
                'City' => $request->ShipAddr_City,
                'CountryCode' => $request->ShipAddr_CountryCode,
                'Country' => $request->ShipAddr_Country,
                'County' => $request->ShipAddr_County,
                'CountrySubDivisionCode' => $request->ShipAddr_CountrySubDivisionCode,
                'PostalCode' => $request->ShipAddr_PostalCode,
                'PostalCodeSuffix' => $request->ShipAddr_PostalCodeSuffix,
                'Lat' => $request->ShipAddr_Lat,
                'Long' => $request->ShipAddr_Long,
                'Tag' => $request->ShipAddr_Tag,
                'Note' => $request->ShipAddr_Note
            ],
            'Notes' => $request->Notes,
        ]);

        $resultingObj = $dataService->Update($resourceObj);

        return response()->json([
            'success' => "Vendor #{$resultingObj->Id} updated",
            'vendor' => $resultingObj
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function vendor_delete($id)
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        $dataService->setLogLocation("./logs");
        $dataService->throwExceptionOnError(true);

        $vendor = $dataService->FindbyId('vendor', $id);

        $resultingObj = $dataService->Delete($vendor);

        return response()->json([
            'success' => "Vendor #{$id} removed",
            'resultingObj' => $resultingObj
        ]);
    }

    /** -------------------------------------------------------------------------
     * QB Vendor Routes
     * --------------------------------------------------------------------------
     * 
     * @return \Illuminate\Http\Response
     * 
     */
    public function vendor_credits()
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        $allVendorCredits = $dataService->FindAll('VendorCredit');

        return response()->json([
            // 'count' => count($allVendorCredits),
            'vendor_credits' => $allVendorCredits
        ]);
    }

    /**
     * SHOW Vendor
     */
    public function vendor_credit_show($id)
    {
        $dataService = $this->getDataService();
        // Check if request is api or not
        if (request()->is('api/*') == 1) {
            if (!$dataService) {
                // if api return a Oauth2 redirect route
                return response()->json([
                    'error' => 'You have authorized your Quickbooks account',
                    'redirect' => url('/qb_login')
                ]);
            }
        } else {
            // if app reoute redirect to OAuth2 route
            if (!$dataService) {
                return redirect('/qb_login');
            }
        }
        $findVendorCredit = $dataService->FindById('VendorCredit', $id);

        return response()->json([
            'vendor_credit' => $findVendorCredit
        ]);
    }
}
