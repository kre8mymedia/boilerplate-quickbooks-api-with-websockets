<?php

namespace App\Http\Controllers;

use App\QBToken;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $token = QBToken::where('user_id', auth()->user()->id)->first();

        if($token) {
            $has_account = "does";
        } else {
            $has_account = "does not";
        }

        return view('home')->with([
            'has_account' => $has_account
        ]);
    }
}
