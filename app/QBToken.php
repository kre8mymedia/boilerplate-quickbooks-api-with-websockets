<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QBToken extends Model
{
    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    |
    */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
