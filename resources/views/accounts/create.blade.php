@extends('layouts.app')

@section('content')
    <div class="container">
        <!-- Messages -->
        @include('inc.messages')
        <!-- END Messages -->
    </div>

    <div class="container">
      <div class="row">
        <div class="col-sm-12">

          <form method="POST" action="/qb_accounts">
            @csrf

            <div class="form-group">
              <label for="Name">Name</label>
              <input type="text" class="form-control" id="Name" name="Name" aria-describedby="AccountName">
            </div>

            <div class="form-group">
              <label for="SubAccount">SubAccount</label>
              <input type="text" class="form-control" id="SubAccount" name="SubAccount">
            </div>

            <div class="form-group">
              <label for="ParentRef">ParentRef</label>
              <input type="text" class="form-control" id="ParentRef" name="ParentRef">
            </div>

            <div class="form-group">
              <label for="Description">Description</label>
              <input type="text" class="form-control" id="Description" name="Description">
            </div>

            <div class="form-group">
              <label for="AccountType">AccountType</label>
              <input type="text" class="form-control" id="AccountType" name="AccountType">
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
          
        </div>
      </div>
    </div>

@endsection
