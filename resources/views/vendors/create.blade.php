@extends('layouts.app')

@section('content')
    <div class="container">
        <!-- Messages -->
        @include('inc.messages')
        <!-- END Messages -->
    </div>

    <div class="container">
      <div class="row">
        <div class="col-sm-12">

          <h2>Create Vendor</h2>

          <form method="POST" action="/qb_vendors">
            @csrf
            <hr>
            <div class="row">
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="GivenName">Firstname</label>
                  <input type="text" class="form-control" id="GivenName" name="GivenName">
                </div>
              </div>
  
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="MiddleName">Middlename</label>
                  <input type="text" class="form-control" id="MiddleName" name="MiddleName">
                </div>
              </div>
  
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="FamilyName">Lastname</label>
                  <input type="text" class="form-control" id="FamilyName" name="FamilyName">
                </div>
              </div>
  
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="Suffix">Suffix</label>
                  <input type="text" class="form-control" id="Suffix" name="Suffix">
                </div>
              </div>
            </div>

            <h3>Contact Details</h3>
            <hr>
            <div class="row">
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="PrimaryEmailAddr">Email</label>
                  <input type="email" class="form-control" id="PrimaryEmailAddr" name="PrimaryEmailAddr">
                </div>
              </div>
  
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="PrimaryPhone">PrimaryPhone</label>
                  <input
                  type="tel"
                  class="form-control"
                  id="PrimaryPhone"
                  name="PrimaryPhone"
                  placeholder="(555) 555-1234"
                  pattern="[0-9]({3}) [0-9]{3}-[0-9]{4}">
                </div>
              </div>
  
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="Mobile">Mobile</label>
                  <input
                  type="tel"
                  class="form-control"
                  id="Mobile"
                  name="Mobile"
                  placeholder="(555) 555-1234"
                  pattern="[0-9]({3}) [0-9]{3}-[0-9]{4}">
                </div>
              </div>
  
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="Fax">Fax</label>
                  <input
                  type="tel"
                  class="form-control"
                  id="Fax"
                  name="Fax"
                  placeholder="(555) 555-1234"
                  pattern="[0-9]({3}) [0-9]{3}-[0-9]{4}">
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-4">
                <div class="form-group">
                  <label for="Title">Title</label>
                  <input type="text" class="form-control" id="Title" name="Title">
                </div>
              </div>
  
              <div class="col-sm-4">
                <div class="form-group">
                  <label for="CompanyName">CompanyName</label>
                  <input type="text" class="form-control" id="CompanyName" name="CompanyName">
                </div>
              </div>
  
              <div class="col-sm-4">
                <div class="form-group">
                  <label for="WebAddr">Website</label>
                  <input type="url" class="form-control" id="WebAddr" name="WebAddr">
                </div>
              </div>
            </div>

            <h3>Billing Address</h3>
            <hr>
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="BillAddr_Line1">Address</label>
                  <input type="text" class="form-control" id="BillAddr_Line1" name="BillAddr_Line1">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="BillAddr_Line2">Line 2</label>
                  <input type="text" class="form-control" id="BillAddr_Line2" name="BillAddr_Line2">
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="BillAddr_City">City</label>
                  <input type="text" class="form-control" id="BillAddr_City" name="BillAddr_City">
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="BillAddr_CountrySubDivisionCode">State</label>
                  <input type="text" class="form-control" id="BillAddr_CountrySubDivisionCode" name="BillAddr_CountrySubDivisionCode">
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="BillAddr_Country">Country</label>
                  <input type="text" class="form-control" id="BillAddr_Country" name="BillAddr_Country">
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="BillAddr_PostalCode">Zip Code</label>
                  <input type="text" class="form-control" id="BillAddr_PostalCode" name="BillAddr_PostalCode">
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="BillAddr_Note">Address Notes</label>
                  <textarea class="form-control" id="BillAddr_Note" name="BillAddr_Note" rows="3"></textarea>
                </div>
              </div>
            </div>

            <h3>Shipping Address</h3>
            <hr>
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="ShipAddr_Line1">Address</label>
                  <input type="text" class="form-control" id="ShipAddr_Line1" name="ShipAddr_Line1">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="ShipAddr_Line2">Line 2</label>
                  <input type="text" class="form-control" id="ShipAddr_Line2" name="ShipAddr_Line2">
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="ShipAddr_City">City</label>
                  <input type="text" class="form-control" id="ShipAddr_City" name="ShipAddr_City">
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="ShipAddr_Country">State</label>
                  <input type="text" class="form-control" id="ShipAddr_Country" name="ShipAddr_Country">
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="ShipAddr_CountrySubDivisionCode">Country</label>
                  <input type="text" class="form-control" id="ShipAddr_CountrySubDivisionCode" name="ShipAddr_CountrySubDivisionCode">
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="ShipAddr_PostalCode">Zip Code</label>
                  <input type="text" class="form-control" id="ShipAddr_PostalCode" name="ShipAddr_PostalCode">
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="ShipAddr_Note">Address Notes</label>
                  <textarea class="form-control" id="ShipAddr_Note" name="ShipAddr_Note" rows="3"></textarea>
                </div>
              </div>
            </div>

            <h3>Vendor Notes</h3>
            <hr>
            <div class="form-group">
              <label for="Notes">Notes</label>
              <textarea class="form-control" id="Notes" name="Notes" rows="3"></textarea>
            </div>
         
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
          
        </div>
      </div>
    </div>

@endsection
