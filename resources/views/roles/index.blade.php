@extends('layouts.app')

@section('content')
    <div class="container">
        <!-- Messages -->
        @include('inc.messages')
        <!-- END Messages -->
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <roles-component></roles-component>
            </div>
        </div>
    </div>
@endsection
