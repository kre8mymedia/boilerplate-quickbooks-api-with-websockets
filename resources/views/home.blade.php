@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-4 mx-auto">
                <!-- Messages -->
                @include('inc.messages')
                <!-- END Messages -->
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6 mx-auto">
                <div class="card mx-auto" style="max-width:400px">
                    <img class="card-img-top" src="https://cdn1.vectorstock.com/i/1000x1000/46/55/person-gray-photo-placeholder-woman-vector-22964655.jpg" alt="Card image">
                    <div class="card-body">
                      <h4 class="card-title">{{auth()->user()->name}}</h4>
                      <p class="card-text">This user <span @if($has_account == 'does') class="badge badge-success" @else class="badge badge-danger" @endif>{{ $has_account }}</span> have a Quickbooks account</p>
                      <a href="/qb_login" class="btn btn-primary">OAuth to Quickbooks</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
