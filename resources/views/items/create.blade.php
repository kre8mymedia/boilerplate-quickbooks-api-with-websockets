@extends('layouts.app')

@section('content')
    <div class="container">
        <!-- Messages -->
        @include('inc.messages')
        <!-- END Messages -->
    </div>

    <div class="container">
      <div class="row">
        <div class="col-sm-12">

          <h2>Create Item</h2>

          <form method="POST" action="/qb_items">
            @csrf
            <hr>
            <div class="row">
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="ExpenseAccountId">ExpenseAccountRef</label>
                  <input type="text" class="form-control" id="ExpenseAccountId" name="ExpenseAccountId">
                </div>
              </div>

              <div class="col-sm-3">
                <div class="form-group">
                  <label for="IncomeAccountId">IncomeAccountRef</label>
                  <input type="text" class="form-control" id="IncomeAccountId" name="IncomeAccountId">
                </div>
              </div>

              <div class="col-sm-3">
                <div class="form-group">
                  <label for="AssetAccountId">AssetAccountRef</label>
                  <input type="text" class="form-control" id="AssetAccountId" name="AssetAccountId">
                </div>
              </div>

              <div class="col-sm-3">
                <div class="form-group">
                  <label for="Name">Item Name</label>
                  <input type="text" class="form-control" id="Name" name="Name">
                </div>
              </div>

              <div class="col-sm-3">
                <div class="form-group">
                  <label for="Type">Item Type</label>
                  <select class="form-control" id="Type" name="Type">
                    <option value="Inventory">Inventory</option>
                    <option value="NonInventory">Non-Inventory</option>
                    <option value="Service">Service</option>               
                  </select>
                </div>
              </div>
  
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="QtyOnHand">QtyOnHand</label>
                  <input type="number" class="form-control" id="QtyOnHand" name="QtyOnHand">
                </div>
              </div>
  
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="PurchaseCost">PurchaseCost</label>
                  <input type="number" class="form-control" id="PurchaseCost" name="PurchaseCost">
                </div>
              </div>

              <div class="col-sm-3">
                <div class="form-group">
                  <label for="UnitPrice">UnitPrice</label>
                  <input type="number" class="form-control" id="UnitPrice" name="UnitPrice">
                </div>
              </div>
  
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="Taxable">Taxable</label>
                  <input type="text" class="form-control" id="Taxable" name="Taxable">
                </div>
              </div>
            </div>
            
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
          
        </div>
      </div>
    </div>

@endsection