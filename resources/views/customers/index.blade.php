@extends('layouts.app')

@section('content')
    <div class="container">
        <!-- Messages -->
        @include('inc.messages')
        <!-- END Messages -->
    </div>

    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          
          <form method="POST" action="/qb_customers">
            @csrf
            <div class="form-group">
              <label for="Name">Name</label>
              <input type="text" class="form-control" id="Name" name="Name">
            </div>
            <div class="form-group">
              <label for="Descritpion">CompanyName</label>
              <input type="text" class="form-control" id="CompanyName" name="CompanyName">
            </div>
            <div class="form-group">
              <label for="GivenName">GivenName</label>
              <input type="text" class="form-control" id="GivenName" name="GivenName">
            </div>
            <div class="form-group">
              <label for="DisplayName">DisplayName</label>
              <input type="text" class="form-control" id="DisplayName" name="DisplayName">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
          
        </div>
      </div>
    </div>

@endsection