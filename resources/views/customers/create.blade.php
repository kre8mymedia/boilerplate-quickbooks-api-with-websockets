@extends('layouts.app')

@section('content')
    <div class="container">
        <!-- Messages -->
        @include('inc.messages')
        <!-- END Messages -->
    </div>

    <div class="container">
      <div class="row">
        <div class="col-sm-12">

          <h2>Create Customer</h2>

          <form method="POST" action="/qb_customers/78">
            @csrf
            @method('PUT')
            <hr>
            <div class="row">
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="GivenName">Firstname</label>
                  <input type="text" class="form-control" id="GivenName" name="GivenName">
                </div>
              </div>
  
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="MiddleName">Middlename</label>
                  <input type="text" class="form-control" id="MiddleName" name="MiddleName">
                </div>
              </div>
  
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="FamilyName">Lastname</label>
                  <input type="text" class="form-control" id="FamilyName" name="FamilyName">
                </div>
              </div>
  
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="Suffix">Suffix</label>
                  <input type="text" class="form-control" id="Suffix" name="Suffix">
                </div>
              </div>
            </div>

            <h3>Contact Details</h3>
            <hr>
            <div class="row">
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="PrimaryEmailAddr">Email</label>
                  <input type="email" class="form-control" id="PrimaryEmailAddr" name="PrimaryEmailAddr">
                </div>
              </div>
  
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="PrimaryPhone">PrimaryPhone</label>
                  <input
                  type="tel"
                  class="form-control"
                  id="PrimaryPhone"
                  name="PrimaryPhone"
                  placeholder="(555) 555-1234"
                  pattern="[0-9]({3}) [0-9]{3}-[0-9]{4}">
                </div>
              </div>
  
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="Mobile">Mobile</label>
                  <input
                  type="tel"
                  class="form-control"
                  id="Mobile"
                  name="Mobile"
                  placeholder="(555) 555-1234"
                  pattern="[0-9]({3}) [0-9]{3}-[0-9]{4}">
                </div>
              </div>
  
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="Fax">Fax</label>
                  <input
                  type="tel"
                  class="form-control"
                  id="Fax"
                  name="Fax"
                  placeholder="(555) 555-1234"
                  pattern="[0-9]({3}) [0-9]{3}-[0-9]{4}">
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-4">
                <div class="form-group">
                  <label for="Title">Title</label>
                  <input type="text" class="form-control" id="Title" name="Title">
                </div>
              </div>
  
              <div class="col-sm-4">
                <div class="form-group">
                  <label for="CompanyName">CompanyName</label>
                  <input type="text" class="form-control" id="CompanyName" name="CompanyName">
                </div>
              </div>
  
              <div class="col-sm-4">
                <div class="form-group">
                  <label for="WebAddr">Website</label>
                  <input type="url" class="form-control" id="WebAddr" name="WebAddr">
                </div>
              </div>
            </div>

            <h3>Billing Address</h3>
            <hr>
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="BillAddr_Line1">Address</label>
                  <input type="text" class="form-control" id="BillAddr_Line1" name="BillAddr_Line1">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="BillAddr_Line2">Line 2</label>
                  <input type="text" class="form-control" id="BillAddr_Line2" name="BillAddr_Line2">
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="BillAddr_City">City</label>
                  <input type="text" class="form-control" id="BillAddr_City" name="BillAddr_City">
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="BillAddr_CountrySubDivisionCode">State</label>
                  <input type="text" class="form-control" id="BillAddr_CountrySubDivisionCode" name="BillAddr_CountrySubDivisionCode">
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="BillAddr_Country">Country</label>
                  <input type="text" class="form-control" id="BillAddr_Country" name="BillAddr_Country">
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="BillAddr_PostalCode">Zip Code</label>
                  <input type="text" class="form-control" id="BillAddr_PostalCode" name="BillAddr_PostalCode">
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="BillAddr_Note">Address Notes</label>
                  <textarea class="form-control" id="BillAddr_Note" name="BillAddr_Note" rows="3"></textarea>
                </div>
              </div>
            </div>

            <h3>Shipping Address</h3>
            <hr>
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="ShipAddr_Line1">Address</label>
                  <input type="text" class="form-control" id="ShipAddr_Line1" name="ShipAddr_Line1">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="ShipAddr_Line2">Line 2</label>
                  <input type="text" class="form-control" id="ShipAddr_Line2" name="ShipAddr_Line2">
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="ShipAddr_City">City</label>
                  <input type="text" class="form-control" id="ShipAddr_City" name="ShipAddr_City">
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="ShipAddr_Country">State</label>
                  <input type="text" class="form-control" id="ShipAddr_Country" name="ShipAddr_Country">
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="ShipAddr_CountrySubDivisionCode">Country</label>
                  <input type="text" class="form-control" id="ShipAddr_CountrySubDivisionCode" name="ShipAddr_CountrySubDivisionCode">
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="ShipAddr_PostalCode">Zip Code</label>
                  <input type="text" class="form-control" id="ShipAddr_PostalCode" name="ShipAddr_PostalCode">
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="ShipAddr_Note">Address Notes</label>
                  <textarea class="form-control" id="ShipAddr_Note" name="ShipAddr_Note" rows="3"></textarea>
                </div>
              </div>
            </div>

            <h3>Booleans</h3>
            <hr>
            <div class="row">
              <div class="col-sm-4">
                <div class="form-group">
                  <label for="PreferredDeliveryMethod">Delivery Method</label>
                  <select class="form-control" id="PreferredDeliveryMethod" name="PreferredDeliveryMethod">
                    <option value="None">None</option>
                    <option value="Print">Print</option>
                    <option value="Email">Email</option>
                  </select>
                </div>
              </div>

              <div class="col-sm-4">
                <div class="form-group">
                  <label for="TaxExemptionReasonId">Tax Exemption Reason</label>
                  <select class="form-control" id="TaxExemptionReasonId" name="TaxExemptionReasonId">
                    <option value="1">Federal Government</option>
                    <option value="2">State Government</option>
                    <option value="3">Local Government</option>
                    <option value="4">Tribal Government</option>
                    <option value="5">Charitable Organization</option>
                    <option value="6">Religious Organziation</option>
                    <option value="7">Educational Organization</option>
                    <option value="8">Hospital</option>
                    <option value="9">Resale</option>
                    <option value="10">Direct Pay Permit</option>
                    <option value="11">Multiple points of use</option>
                    <option value="12">Direct Mail</option>
                    <option value="13">Agriculture Production</option>
                    <option value="14">Industrial production/manufacturing</option>
                    <option value="15">Foreign Diplomat</option>                    
                  </select>
                </div>
              </div>

              <div class="col-sm-2">
                <div class="form-group">
                  <label for="Taxable">Taxable</label>
                  <input placeholder="True or False" type="text" class="form-control" id="Taxable" name="Taxable">
                </div>
              </div>

              <div class="col-sm-2">
                <div class="form-group">
                  <label for="IsProject">IsProject</label>
                  <input placeholder="True or False" type="text" class="form-control" id="IsProject" name="IsProject">
                </div>
              </div>
            </div>

            <h3>Customer Notes</h3>
            <hr>
            <div class="form-group">
              <label for="Notes">Notes</label>
              <textarea class="form-control" id="Notes" name="Notes" rows="3"></textarea>
            </div>
         
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
          
        </div>
      </div>
    </div>

@endsection
