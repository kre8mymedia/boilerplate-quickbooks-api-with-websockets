@extends('layouts.app')

@section('content')
    <div class="container">
        <!-- Messages -->
        @include('inc.messages')
        <!-- END Messages -->
    </div>

    <div class="container">
      <div class="row">
        <div class="col-sm-12">

          <form method="POST" action="/qb_bills/{{ $bill->Id }}">
            @csrf
            @method('PUT')
            <div class="form-group">
              <label for="VendorRef">VendorRef</label>
              <input value="{{ $bill->VendorRef }}" type="text" class="form-control" id="VendorRef" name="VendorRef">
            </div>

            <div class="form-group">
              <label for="DetailType">DetailType</label>
              <input value="{{ $bill->Line->DetailType }}" type="text" class="form-control" id="DetailType" name="DetailType">
            </div>

            <div class="form-group">
              <label for="Amount">Amount</label>
              <input value="{{ $bill->Line->Amount }}" type="number" class="form-control" id="Amount" name="Amount" step="0.01">
            </div>

            <div class="form-group">
              <label for="CustomerRef">CustomerRef</label>
              <input value="{{ $bill->Line->AccountBasedExpenseLineDetail->CustomerRef }}" type="text" class="form-control" id="CustomerRef" name="CustomerRef">
            </div>

            <div class="form-group">
              <label for="AccountRef">AccountRef</label>
              <input value="{{ $bill->Line->AccountBasedExpenseLineDetail->AccountRef }}" type="text" class="form-control" id="AccountRef" name="AccountRef">
            </div>

            <div class="form-group">
              <label for="PrivateNote">PrivateNote</label>
              <textarea class="form-control" id="PrivateNote" name="PrivateNote" rows="3">{{ $bill->PrivateNote }}</textarea>
            </div>

            <div class="form-group">
              <label for="DueDate">DueDate</label>
              <input value="{{ $bill->DueDate }}" type="text" class="form-control" id="DueDate" name="DueDate">
            </div>

            <div class="form-group">
              <label for="BillEmail">BillEmail</label>
              <input type="text" class="form-control" id="BillEmail" name="BillEmail">
            </div>

            <div class="form-group">
              <label for="ReplyEmail">ReplyEmail</label>
              <input type="text" class="form-control" id="ReplyEmail" name="ReplyEmail">
            </div>

            <div class="form-group">
              <label for="DocNumber">DocNumber</label>
              <input value="{{ $bill->DocNumber }}" type="text" class="form-control" id="DocNumber" name="DocNumber">
            </div>

            <div class="form-group">
              <label for="Description">Description</label>
              <textarea class="form-control" id="Description" name="Description" rows="3">{{ $bill->Line->Description }}</textarea>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
          
        </div>
      </div>
    </div>

@endsection
